# Datacard example for combine tutorial 2023 (part 1)
---------------------------------------------
imax 1
jmax 1
kmax *
---------------------------------------------

shapes      sig          X      workspace_sig_with_norm_X.root      workspace_sig:model_sig_X
shapes      bkg_mass     X      workspace_bkg_X.root      workspace_bkg:model_bkg_X
shapes      data_obs     X      workspace_bkg_X.root      workspace_bkg:data_X

---------------------------------------------
bin             X
observation     -1
---------------------------------------------
bin             X         X
process         sig          bkg_mass
process         0            1
rate            138000       1.0
---------------------------------------------
